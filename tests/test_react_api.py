import requests

from utils.config import base_api_url


def test_get_status_200():
    response = requests.get(base_api_url)
    assert response.status_code == 200


def test_add_new_item(new_item_body, new_item):
    assert new_item["title"] == new_item_body["title"]
    assert new_item["description"] == new_item_body["description"]
    assert new_item["is_complete"] is False
    assert new_item["_id"]
    assert new_item_body["due_date"] in new_item["due_date"]


def test_get_item(new_item):
    response_get = requests.get(base_api_url)
    response_get_body = response_get.json()
    assert new_item in response_get_body


def test_close_item(new_item_body, new_item):
    updated_item_body = new_item_body
    updated_item_body["is_complete"] = True
    response_updated_item = requests.patch(f"{base_api_url}/{new_item['_id']}", json=updated_item_body)
    assert response_updated_item.status_code == 200
    response_get = requests.get(base_api_url)
    response_get_body = response_get.json()
    assert response_updated_item not in response_get_body
