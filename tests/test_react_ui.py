from pages.add_item_page import AddItemPage
from pages.main_page import MainPage
from utils.helpers import change_to_us_date


def test_add_new_item(driver, new_item_body):
    main_page = MainPage(driver)
    main_page.open_add_item_page()
    add_item_page = AddItemPage(driver)
    add_item_page.add_title(new_item_body["title"])
    add_item_page.add_description(new_item_body["description"])
    add_item_page.add_date(change_to_us_date(new_item_body["due_date"]))
    add_item_page.save_item()
    assert new_item_body in main_page.get_item_list()


def test_close_item(driver, new_item, new_item_body):
    main_page = MainPage(driver)
    main_page.close_item(new_item["title"])
    assert new_item_body not in main_page.get_item_list()
