import pytest
import requests
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from utils.config import base_api_url, base_ui_url
from utils.helpers import generate_random_string, generate_random_date


@pytest.fixture
def driver():
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get(base_ui_url)
    yield driver
    driver.quit()


@pytest.fixture
def new_item_body() -> dict:
    new_item = {
        "title": generate_random_string("title", 10),
        "description": generate_random_string(),
        "due_date": generate_random_date()
    }
    return new_item


@pytest.fixture
def new_item(new_item_body: dict) -> dict:
    response = requests.post(base_api_url, json=new_item_body)
    response_body = response.json()
    yield response_body
    updated_item_body = new_item_body
    updated_item_body["is_complete"] = True
    requests.patch(f"{base_api_url}/{response_body['_id']}", json=updated_item_body)
