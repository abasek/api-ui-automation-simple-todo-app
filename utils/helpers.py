import datetime
import random
import string


def change_to_us_date(word: str) -> str:
    month = str(word)[5:7]
    day = str(word)[8:10]
    year = str(word)[:4]
    return f"{month}/{day}/{year}"


def generate_random_string(prefix="", limit=30) -> str:
    letters = string.ascii_letters
    random_string = ''.join(random.choice(letters) for i in range(limit))
    return prefix + random_string


def generate_random_date() -> str:
    start_date = datetime.date(1900, 1, 1)
    end_date = datetime.date(2025, 2, 1)
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    random_date = start_date + datetime.timedelta(days=random_number_of_days)
    return random_date.strftime("%Y-%m-%d")


def generate_item_list_based_on_items_text(item_text):
    item_list = []
    for item in item_text:
        item_details = {"title": item[:item.index("\n")], "description": item[item.index("Z\n") + 2:],
                        "due_date": item[item.index("Due: ") + 5:item.index("T00")]}
        item_list.append(item_details)
    return item_list
