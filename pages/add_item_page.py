from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class AddItemPage:
    _title_field = (By.CSS_SELECTOR, "input#title")
    _description_field = (By.CSS_SELECTOR, "textarea#description")
    _date_field = (By.CSS_SELECTOR, "input#duedate")
    _save_button = (By.XPATH, "//button[text()='Save']")

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def add_title(self, title: str):
        self.driver.find_element(*self._title_field).send_keys(title)

    def add_description(self, description: str):
        self.driver.find_element(*self._description_field).send_keys(description)

    def add_date(self, date: str):
        self.driver.find_element(*self._date_field).send_keys(date)

    def save_item(self):
        self.driver.find_element(*self._save_button).click()
        WebDriverWait(self.driver, 1).until(EC.invisibility_of_element_located(self._save_button))
