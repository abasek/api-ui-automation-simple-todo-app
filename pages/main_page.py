from selenium.common import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from utils.helpers import generate_item_list_based_on_items_text


class MainPage:
    _add_item_button = (By.CSS_SELECTOR, "button[class='btn btn-primary']")
    _close_item_checkbox = ''
    _item_title = (By.CSS_SELECTOR, 'ul.list-group a div h5')
    _item_desc = (By.CSS_SELECTOR, 'ul.list-group a p')
    _item_date = (By.CSS_SELECTOR, 'ul.list-group a small')
    _item_details = (By.CSS_SELECTOR, 'ul.list-group a')
    _page_body = (By.CSS_SELECTOR, 'body')

    _close_checkbox_by_description_template = "//h5[text()='{}']/preceding-sibling::div"

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def open_add_item_page(self):
        self.driver.find_element(*self._add_item_button).click()

    def get_item_list(self) -> list[dict]:
        self.driver.refresh()
        items = self.driver.find_elements(*self._item_details)
        items_text = [item.text for item in items]
        return generate_item_list_based_on_items_text(items_text)

    def close_item(self, item_title: str):
        item_close_checkbox_selector = (By.XPATH, self._close_checkbox_by_description_template.format(item_title))
        ignored_exceptions = (NoSuchElementException, StaleElementReferenceException)
        self.driver.refresh()
        WebDriverWait(self.driver, 2, ignored_exceptions=ignored_exceptions).until(
            EC.visibility_of_element_located(item_close_checkbox_selector))
        self.driver.find_element(*item_close_checkbox_selector).click()
